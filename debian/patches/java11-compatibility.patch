Description: Fixes the compatibility with Java 11. The patch can be removed after upgrading to the version 4.8
Origin: backport, https://github.com/gradle/gradle/commit/ac15612d41b43c39c8e39d12fdd6621589b0f782
                  https://github.com/gradle/gradle/commit/028548460bd929fd034a552704798ad7f689493a
                  https://github.com/gradle/gradle/commit/3db6e256987053171178aa96a0ef46caedc8d1a4
--- a/subprojects/base-services/src/main/java/org/gradle/internal/classloader/ClassLoaderUtils.java
+++ b/subprojects/base-services/src/main/java/org/gradle/internal/classloader/ClassLoaderUtils.java
@@ -24,6 +24,9 @@
 
 import javax.annotation.Nullable;
 import java.io.IOException;
+import java.lang.invoke.MethodHandle;
+import java.lang.invoke.MethodHandles;
+import java.lang.invoke.MethodType;
 import java.lang.reflect.Field;
 import java.net.MalformedURLException;
 import java.net.URL;
@@ -31,16 +34,15 @@
 
 public abstract class ClassLoaderUtils {
 
-    private static final Unsafe UNSAFE;
+    private static MethodHandle defineClassMethodHandle;
 
     static {
         try {
-            Field theUnsafe = Unsafe.class.getDeclaredField("theUnsafe");
-            theUnsafe.setAccessible(true);
-            UNSAFE = (Unsafe) theUnsafe.get(null);
-        } catch (NoSuchFieldException e) {
-            throw new RuntimeException(e);
-        } catch (IllegalAccessException e) {
+            MethodHandles.Lookup baseLookup = MethodHandles.lookup();
+            MethodType defineClassMethodType = MethodType.methodType(Class.class, new Class[]{String.class, byte[].class, int.class, int.class});
+            MethodHandles.Lookup lookup = MethodHandles.privateLookupIn(ClassLoader.class, baseLookup);
+            defineClassMethodHandle = lookup.findVirtual(ClassLoader.class, "defineClass", defineClassMethodType);
+        } catch (Throwable e) {
             throw new RuntimeException(e);
         }
     }
@@ -101,6 +103,10 @@
     }
 
     public static <T> Class<T> define(ClassLoader targetClassLoader, String className, byte[] clazzBytes) {
-        return Cast.uncheckedCast(UNSAFE.defineClass(className, clazzBytes, 0, clazzBytes.length, targetClassLoader, null));
+        try {
+            return (Class) defineClassMethodHandle.bindTo(targetClassLoader).invokeWithArguments(className, clazzBytes, 0, clazzBytes.length);
+        } catch (Throwable e) {
+            throw new RuntimeException(e);
+        }
     }
 }
--- a/subprojects/base-services/src/main/java/org/gradle/api/JavaVersion.java
+++ b/subprojects/base-services/src/main/java/org/gradle/api/JavaVersion.java
@@ -17,25 +17,26 @@
 
 import com.google.common.annotations.VisibleForTesting;
 
-import java.util.regex.Matcher;
-import java.util.regex.Pattern;
+import java.util.ArrayList;
+import java.util.List;
 
 /**
  * An enumeration of Java versions.
+ * Before 9: http://www.oracle.com/technetwork/java/javase/versioning-naming-139433.html
+ * 9+: http://openjdk.java.net/jeps/223
  */
 public enum JavaVersion {
-    VERSION_1_1(false), VERSION_1_2(false), VERSION_1_3(false), VERSION_1_4(false),
-    // starting from here versions are 1_ but their official name is "Java 6", "Java 7", ...
-    VERSION_1_5(true), VERSION_1_6(true), VERSION_1_7(true), VERSION_1_8(true), VERSION_1_9(true), VERSION_1_10(true);
+    VERSION_1_1, VERSION_1_2, VERSION_1_3, VERSION_1_4,
+    VERSION_1_5, VERSION_1_6, VERSION_1_7, VERSION_1_8,
+    VERSION_1_9, VERSION_1_10, VERSION_11, VERSION_HIGHER;
+    // Since Java 9, version should be X instead of 1.X
+    // However, to keep backward compatibility, we change from 11
+    private static final int FIRST_MAJOR_VERSION_ORDINAL = 10;
     private static JavaVersion currentJavaVersion;
-    private final boolean hasMajorVersion;
     private final String versionName;
-    private final String majorVersion;
 
-    JavaVersion(boolean hasMajorVersion) {
-        this.hasMajorVersion = hasMajorVersion;
-        this.versionName = name().substring("VERSION_".length()).replace('_', '.');
-        this.majorVersion = name().substring(10);
+    JavaVersion() {
+        this.versionName = ordinal() >= FIRST_MAJOR_VERSION_ORDINAL ? getMajorVersion() : "1." + getMajorVersion();
     }
 
     /**
@@ -54,22 +55,18 @@
         }
 
         String name = value.toString();
-        Matcher matcher = Pattern.compile("(\\d{1,2})(\\D.+)?").matcher(name);
-        if (matcher.matches()) {
-            int index = Integer.parseInt(matcher.group(1)) - 1;
-            if (index > 0 && index < values().length && values()[index].hasMajorVersion) {
-                return values()[index];
-            }
-        }
 
-        matcher = Pattern.compile("1\\.(\\d{1,2})(\\D.+)?").matcher(name);
-        if (matcher.matches()) {
-            int versionIdx = Integer.parseInt(matcher.group(1)) - 1;
-            if (versionIdx >= 0 && versionIdx < values().length) {
-                return values()[versionIdx];
-            }
+        int firstNonVersionCharIndex = findFirstNonVersionCharIndex(name);
+
+        String[] versionStrings = name.substring(0, firstNonVersionCharIndex).split("\\.");
+        List<Integer> versions = convertToNumber(name, versionStrings);
+
+        if (isLegacyVersion(versions)) {
+            assertTrue(name, versions.get(1) > 0);
+            return getVersionForMajor(versions.get(1));
+        } else {
+            return getVersionForMajor(versions.get(0));
         }
-        throw new IllegalArgumentException(String.format("Could not determine java version from '%s'.", name));
     }
 
     /**
@@ -90,11 +87,7 @@
     }
 
     public static JavaVersion forClassVersion(int classVersion) {
-        int index = classVersion - 45; //class file versions: 1.1 == 45, 1.2 == 46...
-        if (index >= 0 && index < values().length) {
-            return values()[index];
-        }
-        throw new IllegalArgumentException(String.format("Could not determine java version from '%d'.", classVersion));
+        return getVersionForMajor(classVersion - 44); //class file versions: 1.1 == 45, 1.2 == 46...
     }
 
     public static JavaVersion forClass(byte[] classData) {
@@ -116,18 +109,22 @@
         return this == VERSION_1_7;
     }
 
-    private boolean isJava8() {
+    public boolean isJava8() {
         return this == VERSION_1_8;
     }
 
-    private boolean isJava9() {
+    public boolean isJava9() {
         return this == VERSION_1_9;
     }
 
-    private boolean isJava10() {
+    public boolean isJava10() {
         return this == VERSION_1_10;
     }
 
+    public boolean isJava11() {
+        return this == VERSION_11;
+    }
+
     public boolean isJava5Compatible() {
         return this.compareTo(VERSION_1_5) >= 0;
     }
@@ -148,21 +145,69 @@
         return this.compareTo(VERSION_1_9) >= 0;
     }
 
-    @Incubating
     public boolean isJava10Compatible() {
         return this.compareTo(VERSION_1_10) >= 0;
     }
 
-    @Override
-    public String toString() {
-        return getName();
+    public boolean isJava11Compatible() {
+        return this.compareTo(VERSION_11) >= 0;
     }
 
-    private String getName() {
+    @Override
+    public String toString() {
         return versionName;
     }
 
     public String getMajorVersion() {
-        return majorVersion;
+        return String.valueOf(ordinal() + 1);
+    }
+
+    private static JavaVersion getVersionForMajor(int major) {
+        return major >= values().length ? JavaVersion.VERSION_HIGHER : values()[major - 1];
+    }
+
+    private static void assertTrue(String value, boolean condition) {
+        if (!condition) {
+            throw new IllegalArgumentException("Could not determine java version from '" + value + "'.");
+        }
+    }
+
+    private static boolean isLegacyVersion(List<Integer> versions) {
+        return 1 == versions.get(0) && versions.size() > 1;
+    }
+
+    private static List<Integer> convertToNumber(String value, String[] versionStrs) {
+        List<Integer> result = new ArrayList<Integer>();
+        for (String s : versionStrs) {
+            assertTrue(value, !isNumberStartingWithZero(s));
+            try {
+                result.add(Integer.parseInt(s));
+            } catch (NumberFormatException e) {
+                assertTrue(value, false);
+            }
+        }
+        assertTrue(value, !result.isEmpty() && result.get(0) > 0);
+        return result;
+    }
+
+    private static boolean isNumberStartingWithZero(String number) {
+        return number.length() > 1 && number.startsWith("0");
+    }
+
+    private static int findFirstNonVersionCharIndex(String s) {
+        assertTrue(s, s.length() != 0);
+
+        for (int i = 0; i < s.length(); ++i) {
+            if (!isDigitOrPeriod(s.charAt(i))) {
+                assertTrue(s, i != 0);
+                return i;
+            }
+        }
+
+        return s.length();
+    }
+
+    private static boolean isDigitOrPeriod(char c) {
+        return (c >= '0' && c <= '9') || c == '.';
     }
 }
